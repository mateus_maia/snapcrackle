function SnapCrackle(maxValue){
    let string = "";

    for(let i=1;i<=maxValue;i++){
        if(i%2!==0){                          //caso em que o número é ímpar
            if(i%5===0){                     //múltiplo de 5 
                string += "SnapCrackle, " ;             
            }else{                           //É impar mas não é múltiplo por 5
                string += "Snap, ";
            }
        }else if(i%5===0){                   //caso em que é par e divisível por 5
            string += "Crackle, ";           
        }else{
            string += i + ", " ;
        }
    }
    console.log(string)
    return string
 }
SnapCrackle(12)
// ========================================================================================

function snapCracklePrime(maxValue){
    let string = "";
    for(let i=1;i<=maxValue;i++){
        if(verificaPrimo(i)){
            if(i%2!==0){                         //caso em que o número é ímpar
                if(i%5===0){                     //múltiplo de 5 
                    string += "SnapCracklePrime, ";             
                }else{                           //É impar mas não é múltiplo por 5
                    string += "SnapPrime, ";
                }
            }else if(i%5===0){                   //caso em que é par e divisível por 5
                string += "CracklePrime, ";           
            }else{
                string += "Prime, ";
            }
        }
        else{
            if(i%2!==0){                        
                if(i%5===0){                     
                    string += "SnapCrackle, " ;             
                }else{                          
                    string += "Snap, ";
                }
            }else if(i%5===0){                   
                string += "Crackle, ";           
            }else{
                string += i + ", " ;
            }
        }
    }
    console.log(string)
    return string
}
snapCracklePrime(15)

function verificaPrimo(numero){
    let cont = 0;
    for(let i=1;i<numero+1;i++){
        if(numero%i===0){
            cont++    
        }
    }
    if(cont===2){
        return true
        // return console.log("primo")
    }else{
        return false
        // return console.log("n é primo")
    }
    
}


// verificaPrimo(2)
